# arena_camera_ros2_docker

This repo builds two Docker images:

* [`registry.gitlab.com/apl-ocean-engineering/arena_camera_ros2_docker:build-latest`](rregistry.gitlab.com/apl-ocean-engineering/arena_camera_ros2_docker:build-latest) adds the [Lucid Vision](https://thinklucid.com/) [Arena SDK](https://thinklucid.com/downloads-hub/) to the [ROS2 "noetic-perception" image](https://hub.docker.com/_/ros/).  The SDK is installed at `/usr/local/ArenaSDK`, and the entrypoint is set to the [`ros_entrypoint.sh`](ros_entrypoint.sh) which automatically sources the ROS environment.

* [`registry.gitlab.com/apl-ocean-engineering/arena_camera_ros_docker:latest`](registry.gitlab.com/apl-ocean-engineering/arena_camera_ros_docker:latest) installs the [`arena_camera_ros` ROS2 node](https://github.com/lucidvisionlabs/arena_camera_ros2) and its dependencies in a catkin workspace at `/root/arena_ws`.   The workspace is built and sources by `ros_entrypoint.sh`.   The default command in the images is
`roslaunch arena_camera arena_camera_nodelet.launch
` which is a minimal example for connecting to a single Lucid Vision camera on the network (see "Running").


## **Dockerfile**

The Docker images are built automatically by Gitlab CI and can be retrieved as:

* [`registry.gitlab.com/apl-ocean-engineering/arena_camera_ros2_docker:latest`](registry.gitlab.com/apl-ocean-engineering/arena_camera_ros2_docker:latest)
* [`registry.gitlab.com/apl-ocean-engineering/arena_camera_ros2_docker:build-latest`](registry.gitlab.com/apl-ocean-engineering/arena_camera_ros2_docker:build-latest)


The Docker images can be built manually with the `./build.sh` script, which will use the latest version of [`arena_camera_ros`](https://github.com/lucidvisionlabs/arena_camera_ros2) from Gitlab.  The following build args can be used to set the Git repo:

* `ARENA_CAMERA_ROS2_REPO` : URL for git repo for `arena_camera_ros2`.  Defaults to the [official Lucid Vision repo on Github](https://github.com/lucidvisionlabs/arena_camera_ros2)
* `ARENA_CAMERA_ROS2_BRANCH` : Branch in repo to checkout



`./build.sh local` will copy the contents of the `local_src` directory into the image before building, allowing development code to be built in the image.


# Running

The default command will run [arena_camera_nodelet.launch](arena_camera_ros2/arena_camera/launch/arena_camera_nodelet.launch).

```
docker run --net host --rm -it registry.gitlab.com/apl-ocean-engineering/arena_camera_ros_docker:latest
```

For more sophisticated launch behavior, other launch or configuration files can be mounted in the image.
